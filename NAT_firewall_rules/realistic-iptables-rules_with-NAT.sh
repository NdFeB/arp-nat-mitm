#!/bin/bash

# FLUSH RULES (not FORWARD as Docker uses it)
iptables -F INPUT
iptables -F OUTPUT
iptables -F FORWARD # comment this if you use docker
iptables -F
iptables -t nat -F
iptables -X

# DEFAULT POLICIES
iptables  -P INPUT   DROP
iptables  -P FORWARD DROP
ip6tables -P INPUT   DROP
ip6tables -P FORWARD DROP
ip6tables -P OUTPUT  DROP

# Loopback
iptables -A INPUT  -i lo -s 127.0.0.1 -d 127.0.0.1 -j ACCEPT
iptables -A OUTPUT -o lo -s 127.0.0.1 -d 127.0.0.1 -j ACCEPT

# Allow ping
iptables -A INPUT  -p icmp -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT

# Drop XMAS & NULL scans
#iptables -A INPUT -p tcp --tcp-flags FIN,URG,PSH FIN,URG,PSH -j DROP
#iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
#iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
#iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP

# ESTABLISHED/RELATED
#iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT   -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

iptables -A INPUT -p tcp --dport 22  -j ACCEPT
iptables -A INPUT -p tcp --dport 445 -j ACCEPT

# If we do NAT and fun things
#iptables -A FORWARD -i eth0 -s 192.168.1.0/24 -j ACCEPT
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
echo 1 > /proc/sys/net/ipv4/ip_forward

# What should do a NAT based MiTM:
iptables -N ACCEPT_VICTIMS
iptables -A FORWARD -j ACCEPT_VICTIMS

# Then for each victim, considering -i as a variable, and -s either var1,var2 either subnet/cidr
iptables -A ACCEPT_VICTIMS -i eth0 -s 10.3.1.0/24 -j ACCEPT

#iptables  -A INPUT   -j LOG
#iptables  -A FORWARD -j LOG
#iptables6 -A INPUT   -j LOG
#iptables6 -A FORWARD -j LOG

exit 0

