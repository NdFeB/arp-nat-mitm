#!/bin/bash

# FLUSH RULES (not FORWARD as Docker uses it)
iptables -F INPUT
iptables -F OUTPUT
iptables -F FORWARD # comment this if you use docker
iptables -F
iptables -t nat -F
iptables -X

iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT

# ESTABLISHED/RELATED
#iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT   -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# If we do NAT and fun things
#iptables -A FORWARD -i eth0 -s 192.168.1.0/24 -j ACCEPT
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

# What should do a NAT based MiTM:
iptables -N ACCEPT_CLIENTS
iptables -A FORWARD -j ACCEPT_CLIENTS

iptables -A ACCEPT_CLIENTS -i eth0 -s 10.3.1.0/24 -j ACCEPT

exit 0

