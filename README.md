# arp-nat-mitm

Scapy script to perform one way mitm attack on LAN, based on arp cache poisoning and NAT.

This attack can be done via 2 main ways: attack both client and router, or only attack client and NAT its trafic to any router.

---

## Attack client and router

Consist to ARP poison both victim client and victim router. It generates twice more trafic than a single target attack, it is easy to detect and many routers implement ARP cache poisoning protection. That is why we are going to focus on single target attack.

---

## Attack client, NAT it to router

Consist to ARP poison the victim client only. The router does not receive any forged ARP packet. Our attacking station will embed a masquerade post-routing config. It will forward packets to the router under its own IP address. Works really well, can be scripted and one lined.

---

## Attack client, NAT it through another router

Instead of NATing packets to the same network (the legitimate router), we can specify a different default route on our attacking machine. We then modify the routing firewall rule to match the new out interface.

Can also be scriptted, can allow clients to bypass firewalls or to change of measured data source seamlessly
